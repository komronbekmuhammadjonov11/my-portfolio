import React from "react";
import LeftContent from "../LeftContent/LeftContent";
import RightContent from "../RightContent/RightContent";
import styles from "./Layout.module.css";

const Layout = () => {
  return (
    <div className={styles.layout}>
      <div className={styles.layout_container}>
        <div className={styles.left_content}>
          <LeftContent />
        </div>

        <div className={styles.right_content}>
          <RightContent />
        </div>
      </div>
    </div>
  );
};

export default Layout;
