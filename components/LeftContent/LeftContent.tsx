import React from "react";
import styles from "./LeftContent.module.css";

const LeftContent = () => {

  const year = new Date().getFullYear();
  return (
    <div className={styles.left_wrapper}>
      <div className={styles.left_wrapper_details}>
        <p className={styles.hi_message}> Hi , There! </p>
        <h1 className={styles.title}>
          I Am <span className={styles.title_mark}>Web Developer.</span>
        </h1>
        <p className={styles.my_description}>
          Hello everyone, my name is Komronbek. I am working as a Front End developer. My current work experience is 1+ years.
          Primary technologies: JavaScript, TypeScript, React and Next.js.
        </p>
        <div className={styles.icon_groups}>
          <a
            href="https://t.me/komronbek_muhammadjonov"
            target="_blank"
            className={styles.icon_bg}
            rel="noreferrer"
          >
            <div
              className={`${styles.icon_content}  ${styles.telegram_icon}`}
            />
          </a>
          <a
            href="https://www.facebook.com/komronbek.muhammadjonov"
            target="_blank"
            className={styles.icon_bg}
            rel="noreferrer"
          >
            <div
              className={`${styles.icon_content}  ${styles.facebook_icon}`}
            />
          </a>
          <a
            href="https://gitlab.com/komronbekmuhammadjonov11"
            target="_blank"
            className={styles.icon_bg}
            rel="noreferrer"
          >
            <div className={`${styles.icon_content}  ${styles.git_icon}`} />
          </a>
          <a
            href="https://www.linkedin.com/in/komronbek11"
            target="_blank"
            className={styles.icon_bg}
            rel="noreferrer"
          >
            <div
              className={`${styles.icon_content}  ${styles.linkedin_icon}`}
            />
          </a>
          <a
            href="https://www.instagram.com/komronbek0204"
            target="_blank"
            className={styles.icon_bg}
            rel="noreferrer"
          >
            <div
              className={`${styles.icon_content}  ${styles.instagram_icon}`}
            />
          </a>
        </div>
        {/*<div className={styles.cv_button_warp}>*/}
        {/*  <a className={styles.cv_button} href="https://tashkent.hh.uz/resume_converter/Muhammadjonov%20Komronbek%20Ummatillo%20og%27li.pdf?hash=d7a9dbe3ff0b7772130039ed1f566e4831536f&type=pdf&hhtmSource=resume&hhtmFrom=resume_list" rel="noopener noreferrer" download > Download CV</a>*/}
        {/*</div>*/}
      </div>
      <div className={styles.bottom_text}>
        © {year} Komronbek — All Rights Reserved.
      </div>
    </div>
  );
};

export default LeftContent;
