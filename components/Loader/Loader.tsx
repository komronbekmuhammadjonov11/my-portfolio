import React from "react";
import styles from "./Loader.module.css";

const Loader = () => {
  return (
    <div className={styles.loaderContainer}>
      <div className={styles.loader}>
        <span className={styles.loaderItem} />
        <span className={styles.loaderItem} />
      </div>
    </div>
  );
};

export default Loader;
