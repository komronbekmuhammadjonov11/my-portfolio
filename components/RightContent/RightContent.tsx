import React from "react";
import styles from "./RightContent.module.css";

const RightContent = () => {
  return (
    <>
      <div className={styles.my_image_wrapper}>
        <div className={styles.my_image} />
      </div>

      <div className={styles.bg_image}></div>
    </>
  );
};

export default RightContent;
