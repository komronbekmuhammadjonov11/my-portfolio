import React from "react";
import MyApp from "./index";

const PageNotFound = () => {
  return <MyApp />;
};

export default PageNotFound;
