import "../styles/globals.css";
import type { AppProps } from "next/app";
import Script from "next/script";
import { Suspense } from "react";
import { Loader } from "../components";
import Head from "next/head";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta
          name="keywords"
          content="portfolio komronbek muhammadjonov  komronbekme kamronbek"
        />
        <meta name="description" content="Frontendchi Komronbek" />
        <meta property="og:description" content="Portfolio" />
        <meta
          property="og:title"
          content="Komronbek Muhammadjonovning portfoliosi"
        />
        <link
          rel="shortcut icon"
          href="https://hotel-kupferhammer.com/wp-content/uploads/2022/03/cropped-Favicon_Zeichenflaeche-1.png"
        />
        <title>Komronbek • Welcome to my portfolio</title>
      </Head>
      <Script
        id="googletagmanager1"
        strategy="lazyOnload"
        src="https://www.googletagmanager.com/gtag/js?id=G-9JE082X6KK"
      />
      <Script id="googletagmanager2" strategy="lazyOnload">
        {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
          
            gtag('config', 'G-9JE082X6KK')
          `}
      </Script>
      <Suspense fallback={<Loader />}>
        <Component {...pageProps} />
      </Suspense>
    </>
  );
}

export default MyApp;
